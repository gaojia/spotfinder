import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { LoginPage } from '../pages/about/login/login'; 
import { RegisterPage } from '../pages/about/register/register'; 
import { ContactPage } from '../pages/contact/contact';
import { MessagePage } from '../pages/message/message';
import { MsgsearchPage } from '../pages/message/msgsearch';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UtilService } from '../providers/util';
import { Globals } from './globals';
import { Data } from "../providers/data";

import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { ConvertTimePipe } from '../pipes/converttime.pipe';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    LoginPage,
    RegisterPage,
    ContactPage,
    MessagePage,
    MsgsearchPage,
    HomePage,
    TabsPage,
    ConvertTimePipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
     tabsHideOnSubPages: 'true',         //ionic3隐藏全部子页面tabs
      mode: 'ios'          //把所有平台设置为iOS风格：
    }),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    LoginPage,
    RegisterPage,
    ContactPage,
    MessagePage,
    MsgsearchPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UtilService,
    HttpModule,
    Globals,
    Data
  ]
})
export class AppModule {}
