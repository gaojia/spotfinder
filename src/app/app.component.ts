import { Component, ViewChild } from '@angular/core';
import { App, Platform, Nav, ToastController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Helper } from '../providers/helper';
import { TabsPage } from '../pages/tabs/tabs';
import { UtilService } from "../providers/util";
import { Data } from "../providers/data";
import { Globals } from './globals';
import { UNREGUSER_NAME } from "../providers/constants";


@Component({
  templateUrl: 'app.html',
})
export class MyApp {
   rootPage:any = TabsPage;
  // //platform: any = Platform;
  // toast: any = ToastController;
  // app: any = App;
  backButtonPressed: boolean = false;  //用于判断返回键是否触发
  helper: Helper;
  errorMessage: string;
  @ViewChild(Nav) nav: Nav;

  constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public toast: ToastController, public app: App, public utilService: UtilService, public globals: Globals, public data: Data, public loadingCtrl: LoadingController,) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.initializeApp(); 
    });
  }

  initializeApp() {
      //双击退出
      this.platform.registerBackButtonAction((): any => {       
        if (!this.app.getActiveNav().canGoBack()) {//判断当前活动导航是否有上一层级:否
          return this.showExit();
        }      
        this.app.getActiveNav().pop();
      }, 101);

      //获取IP
      this.utilService.getIP().subscribe(
        ipdata => {
          this.globals.MYIP = <any>ipdata;
        },
        error => this.errorMessage = <any>error,
        function complete(){
        },
      );
      
      //获取或者创建新的昵称
      this.utilService.getStorage('MYNICKNAME').then((nickname) => {
        if (nickname == undefined){
          //alert("没名字啊，创建游客名");
          this.globals.MYNICKNAME = UNREGUSER_NAME ;
          //this.utilService.setStorage('MYNICKNAME', '尼克');
        } else {
          this.globals.MYNICKNAME = nickname;
        }
      }); 

      //loading开始
      let loading = this.loadingCtrl.create({
        spinner: 'circles',
        content: 'Loading Please Wait...'
      });
      loading.present();
      //获取位置信息
      this.data.getLocation().then((pos:any)  => {
        this.globals.MYLNG = pos.point.lng;
        this.globals.MYLAT = pos.point.lat;
        this.globals.MYCITY = pos.address.city;
        this.globals.MYGEOCODE = this.data.geoEncode(pos.point.lng, pos.point.lat);
        //位置信息读取到后，loading可以消失
        loading.dismiss();
        //alert(JSON.stringify(pos));
      });

  }

  //双击退出函数
  showExit() {
    if (this.backButtonPressed){ 
      this.platform.exitApp();
    } else {      
      let toast = this.toast.create({
        message: '再按一次退出应用',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
      this.backButtonPressed = true;
      setTimeout(() => {
        this.backButtonPressed = false;
      }, 2000)
    }
  }
}
