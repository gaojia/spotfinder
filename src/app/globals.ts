import { Injectable } from '@angular/core';

@Injectable()
export class Globals{
    MYIP = '0.0.0.0'; //1
    MYNICKNAME = ''; //0
    MYID = 0; //0
    MYLNG = 0; //2
    MYLAT = 0; //2
    MYLOGTIME = 0; //1
    MYCITY = '火星'; //2
    MYGEOCODE = ''; //2
    //RegInfo includes regId, reglng, reglat, geocode, regtime, regip
    //0 means 不变除非登录状态改变， 1 means 每次启动都会改变， 2 means 每次刷新都会改变 
}