import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, ViewController, AlertController, NavParams } from 'ionic-angular';
import { RegistersubPage } from '../register/registersub';
import { LoginPage } from '../login/login';
import { HomePage } from '../../home/home';
import { Helper } from '../../../providers/helper';
import { Data } from '../../../providers/data';
import {Http, Headers, RequestOptions}from '@angular/http';

/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  btnText: string = "获取验证码";
  inputYZM: string = '';
  inputPhoneNumber: number ;
  inputPassword : string = '';
  //private checkNumberUrl: string = "";
  requestYZMUrl: string = '/appsendsmscode';
  
  //设置请求方式并请求接口
  headers: Headers;
  options: RequestOptions;
  helper: Helper;

  constructor(private http: Http, 
              private data: Data,
              private toastCtrl:ToastController,
              private loadingCtrl:LoadingController,
              public navCtrl: NavController, 
              private alertCtrl: AlertController,
              private viewCtrl:ViewController,
              public navParams: NavParams) {
              this.helper = new Helper(navCtrl, toastCtrl, null);
              }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

 //计时器
  setTime(count: number) {
    setTimeout(() => this.showDelay(count - 1), 1000);
    var element = <HTMLInputElement> document.getElementById("btnExcel");
    element.disabled = true;
  }

  //延迟执行方法
  showDelay(num: number) {
    this.btnText = num + "秒后重新发送"
    if (num < 1) {
      var element = <HTMLInputElement> document.getElementById("btnExcel");
      element.disabled = false;
      this.btnText = "重新发送";
      return;
    }
    this.setTime(num);
  }


  checkYZMFormat(yzm: string) : boolean {
    if ((/^\d{6}$/.test(yzm))) {
      return true
    }
    return false;
  }

  goToLoginPage(){
    this.navCtrl.push(LoginPage);
  }

  register(){
      //判断手机号
      if (this.inputPhoneNumber == null || !this.helper.checkPhoneFormat(this.inputPhoneNumber.toString())) {
          this.helper.presentToast('手机号为空，或者格式不对！');
          return ;
      }
      //判断密码
      if (this.inputPassword == null || this.inputPassword.toString().length < 6) {
        this.helper.presentToast('密码为空，或者格式不对！');
          return ;
      }
      //判断验证码
      if (this.inputYZM == null || !this.checkYZMFormat(this.inputYZM.toString())) {
        this.helper.presentToast('验证码为空，或者格式不对！');
          return ;
      }

    if (sessionStorage.length == 0) {
      this.helper.presentToast('请点重新发送，进行验证码重新验证');
    } else {
      if (this.inputPhoneNumber.toString() != sessionStorage.getItem("RecNum"))
      {
          this.helper.presentToast('您输入的手机号码不正确哦！');
      } else if ( this.inputYZM != sessionStorage.getItem("Code")) {
          this.helper.presentToast('您输入的验证码不正确哦！');
      } else if (Number(sessionStorage.getItem("TimeStamp")) > Date.parse(new Date().toString())/1000 && Number(sessionStorage.getItem("TimeStamp")) < Date.parse(new Date().toString())/1000 + 600) {
          this.helper.presentToast('您的验证码已经失效，请点重新发送！');
      } else {
          //注册手机号
          //登陆Loading
          let regLoading = this.loadingCtrl.create({
              spinner: "circles",
              content: "正在注册"
          });

          regLoading.present();
          this.data.createUserWithPhoneAndPasswordByWilddog(this.inputPhoneNumber.toString(), this.inputPassword).then((userData:any) => {
            console.log("用户数据:" + JSON.stringify(userData));
            regLoading.dismiss();
            sessionStorage.clear();
            this.navCtrl.push(RegistersubPage, {'uid':userData.uid, 'phone':this.inputPhoneNumber.toString(), 'password':this.inputPassword});
          }).catch((error) => {
              this.helper.presentToast('似乎这个号码注册过，直接登陆!');
              //先登录
              this.data.signInWithPhoneAndPasswordByWilddog(this.inputPhoneNumber.toString(), this.inputPassword).then((res) => {
                console.log("用户数据:" + JSON.stringify(res));
                regLoading.dismiss();
                this.navCtrl.push(HomePage);
              }).catch((error) => {
                  //验证登录密码不正确，等待框体结束，并有提示更改密码的提示
                  console.log("Login Failed:", error);
                  regLoading.dismiss();
                  this.presentConfirm();
              });
          });
      }
    }
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: '确认更改',
      message: '您的手机号已注册过“不约”账号，但是您似乎忘记了您当时的密码，点击重设密码，将进入重设密码页面，重新设置您的登录手机密码',
      buttons: [
        {
          text: '取消',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            //reset all form data
            this.inputPhoneNumber = null;
            this.inputPassword = "";
            this.inputYZM = "";
          }
        },
        {
          text: '去重置',
          handler: () => {
            //更改手机用户密码，并登录
            //this.navCtrl.push(LoginSubPage);
            let alert = this.alertCtrl.create({
              title: '重设密码页面',
              subTitle: '进入重新设置密码页面',
              buttons: ['Dismiss']
            });
            alert.present();
          }
        }
      ]
    });
    alert.present();
  }

  //用于处理请求中body格式
  headleAjaxBodyParam(param) {
    let bodyStr = [];
    for (let item in param) {
      bodyStr.push(item + '=' + param[item])
    }
    return bodyStr.join('&');
  }
  
  //生成六位数字
  generateDigits(){
    var Num = "";
    for (var i=0;i<6;i++) {
      Num += Math.floor(Math.random()*10);
    }
    return Num;
  }
    //发送验证码方法
  sendYZM(count: number) {
    //判断手机是否已注册
    if (this.inputPhoneNumber != null && this.helper.checkPhoneFormat(this.inputPhoneNumber.toString())) {
      let loginLoading = this.loadingCtrl.create({
          spinner: "circles",
          content: "请稍等"
      });
      loginLoading.present();

      // this.http.post(this.checkNumberUrl, this.headleAjaxBodyParam({
      //   "phone": this.inputPhoneNumber
      // }), this.options)
      //   .subscribe((data) => {
      //       let result = data.json();
      //       if (result.status) {
            this.options = new RequestOptions({ 
              headers: new Headers({
			          'Content-Type': 'application/x-www-form-urlencoded'
              })
            });
            var code = this.generateDigits();
            console.log("验证码为:"+code);
            this.http.post(this.requestYZMUrl, this.headleAjaxBodyParam({
              'RecNum': this.inputPhoneNumber,
              'Code': code
            }), this.options)
              .subscribe((data)=> {
                  let result = data.json();
                  console.log(result);
                  if (result.code != null) {
                    this.helper.presentToast('验证码发送成功，请查看填写手机的短信');
                    loginLoading.dismiss();
                    this.setTime(count);//验证码发送成功 开始倒计时
                    //把验证码、手机号、失效时间全部存储至sessionStorage中
                    var timestamp = Date.parse(new Date().toString());  
                    timestamp = timestamp / 1000; 
                    sessionStorage.setItem("Code",code);
                    sessionStorage.setItem("RecNum",this.inputPhoneNumber.toString());
                    sessionStorage.setItem("TimeStamp",timestamp.toString());                    
                  } else {
                    if (result.sub_msg == null) {
                      this.helper.presentToast('短信发送失败,失败原因：'+ result.msg);
                    } else {
                      this.helper.presentToast('短信发送失败,失败原因：'+ result.sub_msg);
                    }
                    
                    loginLoading.dismiss();
                  }
                },
                (err)=> {
                  this.helper.presentToast('网络请求失败,请确保你的网络环境正常');
                  loginLoading.dismiss();
                });

          //   } else {
          //     this.presentToast('手机已被注册');
          //     loginLoading.dismiss();
          //   }
          // },
          // (err) => {
          //   this.presentToast('网络请求失败,请确保你的网络环境正常');
          //   loginLoading.dismiss();
          // });
    } else {
      this.helper.presentToast('请输入正确的手机号');
    }
  }

}
