import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController, ViewController, ModalController, NavParams } from 'ionic-angular';
import { Helper } from '../../../providers/helper';
import { Data } from '../../../providers/data';
import { RegisterPage } from '../register/register';
import { HomePage } from '../../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

    user:any;
    inputPhoneNumber: number ;
    inputPassword : string = '';
    helper: Helper;

  constructor(private navCtrl: NavController, 
              private toastCtrl:ToastController,
              private loadingCtrl:LoadingController,
              private viewCtrl:ViewController,
              private modalCtrl:ModalController,
              private data: Data,
              private navParams: NavParams) {
    this.helper = new Helper(navCtrl, toastCtrl, null);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  /**
   * 用户登录
   */
  login() {
      //判断手机号
      if (this.inputPhoneNumber == null || !this.helper.checkPhoneFormat(this.inputPhoneNumber.toString())) {
          this.helper.presentToast('手机号为空，或者格式不对！');
          return ;
      }
      //判断密码
      if (this.inputPassword == null || this.inputPassword.toString().length < 6) {
        this.helper.presentToast('密码为空，或者小于6位！');
          return ;
      }

      //用户名和密码格式正确后处理
        //登陆Loading
        let loginLoading = this.loadingCtrl.create({
            spinner: "circles",
            content: "正在登录"
        });

        loginLoading.present();
        
        this.data.signInWithPhoneAndPasswordByWilddog(this.inputPhoneNumber.toString(), this.inputPassword).then((userData) => {
            //{"uid":"07e1d8df729fe8aaede92a140d40","displayName":"","phone":"","email":"13426390601@163.com","photoURL":"","providerId":"password","isAnonymous":false,"emailVerified":false,"phoneVerified":false,"providerData":[{"providerId":"password","federatedId":"13426390601@163.com","email":"13426390601@163.com","phoneNumber":"","displayName":"","photoURL":""}],"refreshToken":null} 
            console.log("用户数据:" + JSON.stringify(userData));
            let homepage = this.modalCtrl.create(HomePage);
            homepage.present();
            loginLoading.dismiss();
        }).catch((error) => {
            let loginErrorFormat = this.toastCtrl.create({
                message: "您的用户名和密码不匹配，换个密码试试吧", //error.code, error
                duration: 2000
            });
            loginLoading.dismiss();
            loginErrorFormat.present();
        });
  }

  goToRegPage(){
    let register = this.modalCtrl.create(RegisterPage);
    register.present();
    //this.navCtrl.push(RegisterPage);
  }

  goBackPage(){
    this.viewCtrl.dismiss();
  }
}
