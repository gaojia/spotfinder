import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Data } from '../../providers/data';
import { RegisterPage } from "./register/register";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController,
              private data: Data) {
  }

   ionViewWillEnter() {
      //add logic: login page or this home page navigator
      this.data.isLogin().then((user) => {
          console.log("user data:" + JSON.stringify(user));
          if (user == null)
          {
              //载入登录页面
              this.navCtrl.push(RegisterPage);
          } 
      });
   }
}
