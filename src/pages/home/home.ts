import {Component, ViewChild, ElementRef} from '@angular/core';
import {NavController} from 'ionic-angular';
import { UtilService } from "../../providers/util";
import { Data } from "../../providers/data";
import { Globals } from '../../app/globals';

declare var BMap;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {
  @ViewChild('container') mapElement: ElementRef;
  container: any;
  constructor(public navCtrl: NavController, public globals:Globals, public data:Data) {
    
  }

  ionViewWillEnter() {
    console.log("IP:" + this.globals.MYIP + " " + "NICK:" + this.globals.MYNICKNAME + " " + "LNG:" + this.globals.MYLNG + " " + "LAT:" + this.globals.MYLAT + " " + "CITY:" + this.globals.MYCITY);
  }
}
