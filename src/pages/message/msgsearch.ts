import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import { UtilService } from "../../providers/util";
import { Helper } from '../../providers/helper';

/**
 * Generated class for the MsgsearchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-msgsearch',
  templateUrl: 'msgsearch.html',
})
export class MsgsearchPage {
  public items: Array<string> = [];
  public helper: Helper ;

  constructor(public navCtrl: NavController, private toastCtrl:ToastController, public navParams: NavParams, public utilService: UtilService,) {
    this.helper = new Helper(navCtrl, toastCtrl, null);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MsgsearchPage');
    this.utilService.getStorage('MYSEARCHWORDLIST1').then((value: Array<string>) => {
      if (value == undefined){
      } else {
        this.items = value;
      }
    }); 

  }

  initializeItems() {
    this.items = [
      'Amsterdam',
      'Bogota',
      "Custom",
      "Doom"
    ];
  }

  submitSearch(ev: any) {
    let val = ev.target.value;
    this.items.push(val) ;
    this.utilService.setStorage("MYSEARCHWORDLIST1", this.items);
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  removeAllSearchHistory() {
    this.utilService.removeStorage("MYSEARCHWORDLIST1");
    this.helper.presentToast('历史搜索记录清除成功...');
    this.items = [];
  }
}
