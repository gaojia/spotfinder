import { Component } from '@angular/core';
import { NavController, ToastController, NavParams,LoadingController } from 'ionic-angular';
import { Data } from '../../providers/data';
import { Helper } from '../../providers/helper';
import {MsgsearchPage } from '../message/msgsearch';

/**
 * Generated class for the MessagePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {

  public content: string = "";
  public items: any;
  public pageNumber: number = 1 ;
  public helper: Helper ;

  constructor(public navCtrl: NavController, private toastCtrl:ToastController, public loadingCtrl: LoadingController, public navParams: NavParams, public data: Data) {
    this.helper = new Helper(navCtrl, toastCtrl, null);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagePage');

  //Loading
    let loading = this.loadingCtrl.create({
        spinner: "circles",
        content: "正在加载中..."
    });
    loading.present();

    this.data.getArticleContent(this.pageNumber).then((res: any) => {
      loading.dismiss();
      this.content = res.json();
      this.items = res.json().posts;
      console.log("items2:" + res.json().posts);
    }).catch((error) => {
        //验证登录密码不正确，等待框体结束，并有提示更改密码的提示
        console.log("Login Failed:", error);
        loading.dismiss();
        this.helper.presentToast('加载失败，请刷新页面...');
    });
  }
  openSearch() {
    this.navCtrl.push(MsgsearchPage);
  }

  openUrl (url: string){
    window.open(url, '_blank', 'location=yes');
  }
  

  doInfinite(infiniteScroll) {
    this.pageNumber ++ ;
    console.log('页码加1：' + this.pageNumber);
    this.data.getArticleContent(this.pageNumber).then((res: any) => {
      this.items = this.items.concat(res.json().posts);
      console.log(this.items.length);
      infiniteScroll.complete();
    }).catch((error) => {
        //验证登录密码不正确，等待框体结束，并有提示更改密码的提示
        console.log("Login Failed:", error);
        this.helper.presentToast('加载失败...');
        infiniteScroll.complete();
    });
  }
}
