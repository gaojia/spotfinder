import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ConvertTimePipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'convertTime',
})
export class ConvertTimePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
       var minute = 1000 * 60;
    var hour = minute * 60;
    var day = hour * 24;
    var halfamonth = day * 15;
    var month = day * 30;
    var year = month * 12 + 5 ;
    var result ;
    var dateTimeStamp = Date.parse(value);

    var now = new Date().getTime();
    var diffValue = now - dateTimeStamp;
    if (diffValue < 0) {
      //若日期不符则弹出窗口告之
      //alert(“结束日期不能小于开始日期！”);
    }

    var yearC = diffValue / year ;
    var monthC = diffValue / month;
    var weekC = diffValue / (7 * day);
    var dayC = diffValue / day;
    var hourC = diffValue / hour;
    var minC = diffValue / minute;
    if (yearC >= 1) {
      result = parseInt(yearC.toString()) + "年前";
    }
    else if (monthC >= 1) {
      result = parseInt(monthC.toString()) + "个月前";
    }
    else if (weekC >= 1) {
      result = parseInt(weekC.toString()) + "周前";
    }
    else if (dayC >= 1) {
      result = parseInt(dayC.toString()) + "天前";
    }
    else if (hourC >= 1) {
      result = parseInt(hourC.toString()) + "小时前";
    }
    else if (minC >= 1) {
      result = parseInt(minC.toString()) + "分钟前";
    } else
      result = "刚发布";
    return result;
  }
}
