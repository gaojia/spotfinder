import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {WILDDOG_AUTHDOMAIN} from "./constants";
//import {Observable} from 'rxjs/Observable'
//import {Storage} from '@ionic/storage';
import 'rxjs/add/operator/map';
import * as wilddog from 'wilddog';
import * as Geohash from 'latlon-geohash';
declare var BMap;

/*
  Generated class for the UtilProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class Data {

  constructor(public http: Http) {
      console.log('Hello Data Provider');
      var config = {
        authDomain: WILDDOG_AUTHDOMAIN,
        syncURL: "https://" + WILDDOG_AUTHDOMAIN
      };
      wilddog.initializeApp(config);
  }

  isLogin() {
    return new Promise(resolve => {
        wilddog.auth().onAuthStateChanged(function (user) {
          resolve(user);
        });
    });
  }

  geoEncode(longitude:number, latitude:number) {
    return Geohash.encode(latitude, longitude,12);
  }

  getLocation() {
    return new Promise(resolve => {
      var geolocation = new BMap.Geolocation();
      geolocation.getCurrentPosition(function (pos){
        resolve(pos);
      });
    });
  }

  getCity(){
    var myCity = new BMap.LocalCity();
    return new Promise(resolve => {
      myCity.get(function (data){
        resolve(data.name);
      });
    });
  }

    /**
     * Wilddog.signInWithPhoneAndPassword()
     * @param phone
     * @param password
     */
    signInWithPhoneAndPasswordByWilddog(phone:string, password:string) {
      return new Promise((resolve, reject)  => {
        wilddog.auth().signInWithPhoneAndPassword(phone, password).then(function(res){
          // 获取用户
          resolve(res);
        }).catch(function (error) {
            // 错误处理
            reject(error);
        });
      });
    }

    /**
     * Wilddog.createwithPhonePassword()
     * @param phone
     * @param password
     */
    createUserWithPhoneAndPasswordByWilddog(phone:string, password:string) {
      return new Promise((resolve, reject)  => {
        wilddog.auth().createUserWithPhoneAndPassword(phone, password).then(function(user){
          // 获取用户
          resolve(user);
        }).catch(function (error) {
            // 错误处理
            reject(error);
        });
      });

        // var ref = new Wilddog('https://plant-book.wilddogio.com');
        // // Log me in
        // ref.authWithPassword({
        //     "email": email,
        //     "password": password
        // }, (error, authData) => {
        //     if (error) {
        //         console.log('Login Failed!', error);
        //     } else {
        //         console.log('Authenticated successfully with payload:', authData);
        //         var userdef = new Wilddog('https://plant-book.wilddogio.com/users/' + authData.uid);
        //         userdef.child('email').set(email);
        //         userdef.once("value", (data) => {
        //             this.viewCtrl.dismiss(data.val());
        //         });
        //     }
        // });
    }
      /**
     * Wilddog.authWithPassword()
     * @param email
     * @param password
     */
    authWithPasswordByWilddog(email:string, password:string) {
      return new Promise((resolve, reject)  => {
        wilddog.auth().signInWithEmailAndPassword(email, password).then(function (user) {
          //继续读取user数据表内的数据然后传递给主页面
          var ref = wilddog.sync().ref("/users/"+user.uid);
          ref.once('value', function(snapshot) {
              resolve(snapshot.val());
            }, function (error) {
              console.log("error:" + error);
              reject(error);
            }
          );
        }).catch(function(error) {
          reject(error);
        });
      });

        // var ref = new Wilddog('https://plant-book.wilddogio.com');
        // // Log me in
        // ref.authWithPassword({
        //     "email": email,
        //     "password": password
        // }, (error, authData) => {
        //     if (error) {
        //         console.log('Login Failed!', error);
        //     } else {
        //         console.log('Authenticated successfully with payload:', authData);
        //         var userdef = new Wilddog('https://plant-book.wilddogio.com/users/' + authData.uid);
        //         userdef.child('email').set(email);
        //         userdef.once("value", (data) => {
        //             this.viewCtrl.dismiss(data.val());
        //         });
        //     }
        // });
    }

    getAgreementContent(url) {
      return new Promise((resolve, reject) => {
        this.http.get(url).subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
      })
    }

    getArticleContent(pageNumber:number) {
      return new Promise((resolve, reject) => {
        this.http.get("http://boy99.cn/api/get_recent_posts?page=" + pageNumber).subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
      })
    }

    logout(){
      return new Promise((resolve, reject)  => {
        wilddog.auth().signOut().then(function() {
            // 退出成功
            console.log("sign-out");
            resolve();
        }).catch(function(error) {
            // 发生错误
            console.log("sign-out-error");
            reject(error);
        });
      });
    }
}
