import { Injectable } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';

/*
  Generated class for the Helper page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Injectable()
export class Helper {

  constructor(public navCtrl: NavController, 
              private toastCtrl:ToastController,
              public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelperPage');
  }

  //调用Toast
  presentToast(text: string, showtime: number = 2000) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: showtime,
      position: 'bottom'
    });
    toast.present();
  }

  

  //检查电话号码格式是否正确
  checkPhoneFormat(phone: string): boolean {
    if ((/^1[3|4|5|7|8]\d{9}$/.test(phone))) {
      return true
    }
    return false;
  }
}
