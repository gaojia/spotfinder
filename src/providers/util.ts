import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {SERVER_IPURL} from "./constants";
import {Observable} from 'rxjs/Observable'
import {Storage} from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the UtilProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UtilService {

  constructor(public http: Http,public storage: Storage) {
    console.log('Hello UtilProvider Provider');
  }

  public getIP(): Observable<string> {
    return this.http.get(SERVER_IPURL) 
     .map(function (res) { return res.json().ip; });
  }

  public setStorage(key: string, value: any){
     //this.storage.set(key, JSON.stringify(value));
     this.storage.set(key, value);
  }

  public getStorage(key: string) {
    return this.storage.get(key);
  }

    public removeStorage(key: string) {
    return this.storage.remove(key);
  }


  // private extractData(res: Response) {
  //   let body = res.json().data;
  //   return body || {};
  // }

  // private handleError(error: Response | any) {
  //   let errMsg: string;
  //   if (error instanceof Response) {
  //     const body = error.json() || '';
  //     const err = body.error || JSON.stringify(body);
  //     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
  //   } else {
  //     errMsg = error.message ? error.message : error.toString();
  //   }
  //   console.error(errMsg);
  //   return Observable.throw(errMsg);
  // }
}
